package app

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
)

/**
 *配置文件
 */
var Config appConfig

/**
 * 配置文件定义
 */
type appConfig struct {
	App struct {
		Pid      string
		Name     string
		Env      string
		TrieFile string
	}

	Logger struct {
		Path     string
		FileName string `gcfg:"file-name"`
	}

	Redis struct {
		Host string
		Port int
		Auth string
	}

	Db struct {
		Host     string
		Port     int
		UserName string `gcfg:"user-name"`
		Password string
		Db       string
	}
}

/**
 * 是否为生产环境
 */
func (this *appConfig) IsProduct() bool {
	return this.App.Env == "product"
}

/**
 * 是否为开发环境
 */
func (this *appConfig) IsDevelop() bool {
	return this.App.Env == "develop"
}

/**
 * 获取运行模式
 */
func (this *appConfig) Mode() string {
	if this.IsProduct() {
		return gin.ReleaseMode
	}

	if this.IsDevelop() {
		return gin.DebugMode
	}

	return gin.TestMode
}

func (this *appConfig) String() string {
	str, _ := json.Marshal(this)
	return string(str)
}
