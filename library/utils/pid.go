package utils

import (
	"fmt"
	"sensitive-word-filter/library/app"
	"syscall"
)

func WritePid(pid int) (bool, error) {
	file, err := FOpen(app.Config.App.Pid, "w")
	if err != nil {
		return false, err
	}

	if _, err = fmt.Fprintf(file, "%d", pid); err != nil {
		return false, err
	}
	return true, nil
}

func GetPid() (int, error) {
	pid, err := ReadAllWithChunk(app.Config.App.Pid)
	if err != nil {
		return 0, err
	}

	return String2Int(pid), nil
}

func ProcessExists(pid int) bool {
	if err := syscall.Kill(pid, 0); err == nil {
		return true
	}
	return false
}
