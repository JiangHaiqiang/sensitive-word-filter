package utils

import (
	"github.com/gin-gonic/gin"
	"io"
	"sensitive-word-filter/library/app"
	"time"
)

/**
 * 设置默认Writer
 */
func SetDefaultLoggerWriter() {
	fileName := app.Config.Logger.Path + "/" + time.Now().Format("2006-01-02-") + app.Config.Logger.FileName
	out, _ := FOpen(fileName, "a")
	gin.DefaultWriter = io.MultiWriter(out)
}
