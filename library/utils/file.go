package utils

import (
	"io"
	"os"
	"syscall"
)

func Exists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

func IsFile(path string) bool {
	return !IsDir(path)
}

/**
 *实现fopen
 * mode支持解析：
 * "r"	只读方式打开，将文件指针指向文件头。
 * "r+"	读写方式打开，将文件指针指向文件头。
 * "w"	写入方式打开，将文件指针指向文件头并将文件大小截为零。如果文件不存在则尝试创建之。
 * "w+"	读写方式打开，将文件指针指向文件头并将文件大小截为零。如果文件不存在则尝试创建之。
 * "a"	写入方式打开，将文件指针指向文件末尾。如果文件不存在则尝试创建之。
 * "a+"	读写方式打开，将文件指针指向文件末尾。如果文件不存在则尝试创建之。
 */
func FOpen(fileName string, mode string) (*os.File, error) {
	switch mode {
	case "r":
		return os.OpenFile(fileName, syscall.O_RDONLY, 0)
	case "r+":
		return os.OpenFile(fileName, syscall.O_RDWR, 0666)
	case "w":
		return os.OpenFile(fileName, syscall.O_WRONLY|syscall.O_CREAT|syscall.O_TRUNC, 0666)
	case "w+":
		return os.OpenFile(fileName, syscall.O_RDWR|syscall.O_CREAT|syscall.O_TRUNC, 0666)
	case "a":
		return os.OpenFile(fileName, syscall.O_WRONLY|syscall.O_CREAT|syscall.O_APPEND, 0666)
	case "a+":
		return os.OpenFile(fileName, syscall.O_RDWR|syscall.O_CREAT|syscall.O_APPEND, 0666)
	}

	return os.OpenFile(fileName, syscall.O_RDONLY, 0)
}

/**
 * 分块读取文件
 */
func ReadAllWithChunk(fileName string) (string, error) {
	f, err := FOpen(fileName, "r")
	if err != nil {
		return "", err
	}

	defer f.Close()

	chunk := make([]byte, 0)
	buf := make([]byte, 1024)

	for {
		n, err := f.Read(buf)
		if err != nil && err != io.EOF {
			return "", err
		}

		if n == 0 {
			break
		}

		chunk = append(chunk, buf[:n]...)
	}

	return string(chunk), nil
}

func Unlink(fileName string) error {
	if Exists(fileName) {
		return os.Remove(fileName)
	}

	return nil
}
