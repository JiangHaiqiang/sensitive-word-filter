package utils

import (
	"encoding/binary"
	"math"
	"strconv"
)

func Int64ToString(val int64) string {
	return strconv.FormatInt(val, 10)
}

func Int2String(val int) string {
	return strconv.Itoa(val)
}

func String2Int(str string) int {
	if val, err := strconv.Atoi(str); err == nil {
		return val
	}
	return 0
}

func String2Int64(str string) int64 {
	if val, err := strconv.ParseInt(str, 10, 64); err == nil {
		return val
	}
	return 0
}

func String2Float64(str string) float64 {
	if val, err := strconv.ParseFloat(str, 64); err == nil {
		return val
	}
	return 0
}

func Float64ToString(f64 float64, scale int) string {
	return strconv.FormatFloat(f64, 'f', scale, 64)
}

func Bytes2Float64(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	return math.Float64frombits(bits)
}

func Float64ToBytes(input float64) []byte {
	bits := math.Float64bits(input)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

/**
 * 字节转兆字节
 */
func Byte2MByte(size uint64, scale int) string {
	return Float64ToString(float64(size)/(1024*1024), scale)
}
