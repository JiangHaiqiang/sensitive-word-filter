package memory

import (
	"runtime"
	"sensitive-word-filter/library/utils"
)

func MemInfo() *runtime.MemStats {
	memStat := new(runtime.MemStats)
	runtime.ReadMemStats(memStat)
	return memStat
}

func MemInfoFormat() map[string]interface{} {
	info := MemInfo()
	detail := make(map[string]interface{})
	detail["Alloc"] = utils.Byte2MByte(info.Alloc, 2) + "M"
	detail["TotalAlloc"] = utils.Byte2MByte(info.TotalAlloc, 2) + "M"
	detail["Sys"] = utils.Byte2MByte(info.Sys, 2) + "M"
	detail["Mallocs"] = utils.Byte2MByte(info.Mallocs, 2) + "M"
	detail["Frees"] = utils.Byte2MByte(info.Frees, 2) + "M"
	detail["Frees"] = utils.Byte2MByte(info.Frees, 2) + "M"
	return detail
}
