package cli

import (
	"fmt"
	"os"
)

func Success(item1 string, itemN ...string) {
	for _, item := range itemN {
		item1 += item
	}
	fmt.Printf("\033[97;42m%s\033[0m\n", item1)
}

func Info(item1 string, itemN ...string) {
	for _, item := range itemN {
		item1 += item
	}
	fmt.Printf("%s\n", item1)
}

func Warn(item1 string, itemN ...string) {
	for _, item := range itemN {
		item1 += item
	}
	fmt.Printf("\033[97;43m%s\033[0m\n", item1)
}

func Error(item1 string, itemN ...string) {
	for _, item := range itemN {
		item1 += item
	}
	fmt.Printf("\033[97;41m%s\033[0m\n", item1)
	os.Exit(0)
}
