package utils

import (
	"fmt"
	"strconv"
)

/**
 * 四舍五入
 */
func Round(str interface{}, scale int) string {
	return fmt.Sprintf("%."+strconv.Itoa(scale)+"f", str)
}
