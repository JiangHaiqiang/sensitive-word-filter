package trie

import (
	"strings"
)

/**
 * 字典树
 */
type Trie struct {
	root *Node
}

/**
 *创建字典树
 */
func NewTrie() *Trie {
	return &Trie{root: newNode()}
}

/**
 * 插入词
 */
func (this *Trie) Insert(words string) {
	node := this.root
	runes := []rune(words)
	for _, key := range runes {
		_, ok := node.children[key]
		if !ok {
			node.children[key] = newNode()
		}
		node = node.children[key]
	}
	node.isEnd = true
}

/**
 * 是否包含敏感词
 */
func (this *Trie) IsContain(words string) bool {
	runes := []rune(words)
	node := this.root
	for _, key := range runes {
		if _, ok := node.children[key]; !ok {
			node = this.root
			continue
		}

		if node.children[key].isEnd {
			return true
		}

		node = node.children[key]
	}

	return false
}

/**
 * 包含哪些敏感词
 */
func (this *Trie) Contains(words string) (list []string) {
	runes := []rune(words)
	node := this.root

	items := make(map[string]int)
	item := ""
	for _, key := range runes {
		if _, ok := node.children[key]; !ok {
			node = this.root
			item = ""
			continue
		}

		item += string(key)
		if node.children[key].isEnd {
			if _, ok := items[item]; ok {
				continue
			}
			items[item] = 1
			list = append(list, item)
			continue
		}
		node = node.children[key]
	}
	return
}

/**
 * 将words中的敏感词替换掉
 */
func (this *Trie) Filter(words string, replace string) string {
	runes := []rune(words)
	node := this.root

	items := make(map[string]int)
	item := ""
	length := 0
	for _, key := range runes {
		if _, ok := node.children[key]; !ok {
			node = this.root
			item = ""
			length = 0
			continue
		}

		length++
		item += string(key)
		if node.children[key].isEnd {
			if _, ok := items[item]; ok {
				continue
			}
			items[item] = 1
			words = strings.Replace(words, item, strings.Repeat(replace, length), -1)
			continue
		}
		node = node.children[key]
	}
	return words
}
