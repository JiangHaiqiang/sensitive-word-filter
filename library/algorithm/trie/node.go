package trie

/**
 * 节点
 */
type Node struct {
	children map[rune]*Node
	isEnd    bool
}

/**
 * 实例化
 */
func newNode() *Node {
	return &Node{
		children: make(map[rune]*Node),
		isEnd:    false,
	}
}
