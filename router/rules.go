package router

import (
	"github.com/gin-gonic/gin"
	"sensitive-word-filter/api/filter"
)

/**
 * 加载路由，并且返回Engine
 */
func Engine() *gin.Engine {
	route := gin.Default()
	route.GET("/filter/contain/:content", filter.Contain)
	route.GET("/filter/contains/:content", filter.Contains)
	route.POST("/filter/filter/:content", filter.Filter)
	return route
}
