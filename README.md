# sensitive-word-filter
目录
===

* [使用](#使用)
* [调用](#调用)
* [服务管理](#服务管理)
* [配置文件](#配置文件)

使用
===

```bash
#将依赖放入vendor
go mod vendor

#启动
go run index.go -s start
```

* [回到目录](#目录)

调用
===

* http://127.0.0.1:8066/filter/contain/中国
* http://127.0.0.1:8066/filter/contains/中国
* http://127.0.0.1:8066/filter/filter/中国

* [回到目录](#目录)

服务管理
======

```bash
#编译
go build
#守护进程启动
./sensitive-word-filter -s start &

#优雅restart
./sensitive-word-filter -s reload

#优雅stop
./sensitive-word-filter -s stop
```

* [回到目录](#目录)

配置文件
======

* 注意：不以`/`开头的路径默认为编译后可执行文件路径

```ini
[app]
name        = 敏感词过滤服务
;进程pid文件
pid         = data/sensitive.pid
;环境配置，可以为【develop,test,product】
env         = develop
;敏感词字典
trieFile    = data/sensitive.txt

[logger]
path        = logs
file-name   = app.log

[redis]
host        = 127.0.0.1
port        = 6379
auth        = ""

[db]
host        = 127.0.0.1
port        = 3306
user-name   = ent
password    = sdf@#4sdfasdfwas
db          = ent
```

* [回到目录](#目录)