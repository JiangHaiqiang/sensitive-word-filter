package initial

import (
	"sensitive-word-filter/library/app"
	"sensitive-word-filter/library/utils"
	"sensitive-word-filter/library/utils/cli"
	"strings"
	"time"
)

var date = time.Now().Day()

func Tick() {
	go buildTree()
}

func buildTree() {
	ticker := time.NewTicker(time.Nanosecond)
	<-ticker.C

	startTime := time.Now()
	content, err := utils.ReadAllWithChunk(app.Config.App.TrieFile)

	if err == nil {
		wordsList := strings.Split(content, "\n")
		for _, words := range wordsList {
			SensitiveTrie.Insert(words)
		}

		cli.Warn("build tree cost: ", time.Since(startTime).String())
	}
}
