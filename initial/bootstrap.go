package initial

import (
	"flag"
	"github.com/gin-gonic/gin"
	"gopkg.in/gcfg.v1"
	"os"
	"os/exec"
	"sensitive-word-filter/library/algorithm/trie"
	"sensitive-word-filter/library/app"
	"sensitive-word-filter/library/utils"
	"sensitive-word-filter/library/utils/cli"
)

var cmdMap = map[string]interface{}{
	"reload": reload,
	"stop":   stop,
}

//敏感词字典树
var SensitiveTrie *trie.Trie = trie.NewTrie()

/**
 * 初始化
 */
func Bootstrap() {
	var command string
	var configFile string
	flag.StringVar(&configFile, "f", "", "配置文件路径")
	flag.StringVar(&command, "s", "", "s只能为[start,stop,reload]")
	flag.Parse()

	if command == "" {
		cli.Error("-s为必传参数，可以为[start,stop,reload]")
	}

	//默认配置文件地址
	if configFile == "" {
		configFile = "config/app.ini"
	}

	if !utils.Exists(configFile) {
		cli.Error("配置文件【" + configFile + "】不存在")
		os.Exit(1)
	}

	err := gcfg.ReadFileInto(&app.Config, configFile)
	if err != nil {
		cli.Warn("加载配置文件【" + configFile + "】出现错误:" + err.Error())
	}

	handlerSignal(command)
	cli.Success("config: ", app.Config.String())

	//设置运行模式
	gin.SetMode(app.Config.Mode())
	//设置默认日志Writer
	utils.SetDefaultLoggerWriter()
}

/**
 * 处理信号
 */
func handlerSignal(command string) {
	if _, ok := cmdMap[command]; !ok {
		if command == "start" {
			return
		}
		cli.Error("s只能为[start,stop,reload]")
		os.Exit(0)
	}

	_, err := utils.Call(cmdMap, command)
	if err != nil {
		cli.Error(err.Error())
	}
	os.Exit(0)
}

/**
 * 优雅重启
 */
func reload() {
	pid, err := utils.GetPid()
	if err != nil {
		cli.Error(err.Error())
	}
	if !utils.ProcessExists(pid) {
		cli.Error("进程不存在")
	}

	cmd := exec.Command("/bin/kill", "-1", utils.Int2String(pid))
	cmd.Run()
}

/**
 * 优雅stop
 */
func stop() {
	pid, err := utils.GetPid()
	if err != nil {
		cli.Error(err.Error())
		os.Exit(0)
	}
	if !utils.ProcessExists(pid) {
		cli.Error("进程不存在")
	}

	cmd := exec.Command("/bin/kill", "2", utils.Int2String(pid))
	cmd.Run()
}
