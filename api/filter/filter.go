package filter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sensitive-word-filter/initial"
)

func Filter(c *gin.Context) {
	result := make(map[string]interface{})
	result["content"] = c.Param("content")
	result["result"] = initial.SensitiveTrie.Filter(result["content"].(string), "*")
	//result["memory"] = memory.MemInfo()
	//result["memoryFormat"] = memory.MemInfoFormat()
	c.JSON(http.StatusOK, result)
}
