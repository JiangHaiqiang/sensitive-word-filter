package filter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sensitive-word-filter/initial"
)

/**
 * 是否包含敏感词
 */
func Contain(c *gin.Context) {
	result := make(map[string]interface{})
	result["content"] = c.Param("content")
	result["isContain"] = initial.SensitiveTrie.IsContain(result["content"].(string))

	c.JSON(http.StatusOK, result)
}
