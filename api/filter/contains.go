package filter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sensitive-word-filter/initial"
)

func Contains(c *gin.Context) {
	result := make(map[string]interface{})
	result["content"] = c.Param("content")
	result["contains"] = initial.SensitiveTrie.Contains(result["content"].(string))

	c.JSON(http.StatusOK, result)
}
