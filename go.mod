module sensitive-word-filter

go 1.13

require (
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/gin-gonic/gin v1.5.0
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/ini.v1 v1.51.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
