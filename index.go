package main

import (
	"github.com/fvbock/endless"
	"os"
	"sensitive-word-filter/initial"
	"sensitive-word-filter/library/app"
	"sensitive-word-filter/library/utils"
	"sensitive-word-filter/library/utils/cli"
	"sensitive-word-filter/router"
	"syscall"
)

func main() {
	initial.Bootstrap()
	engine := router.Engine()
	initial.Tick()

	server := endless.NewServer(":8066", engine)
	server.BeforeBegin = func(add string) {
		pid := syscall.Getpid()
		cli.Success(app.Config.App.Name, "启动成功，进程pid：", utils.Int2String(pid))
		_, err := utils.WritePid(pid)
		if err != nil {
			cli.Warn("进程pid写入失败")
		}
	}

	err := server.ListenAndServe()
	if err != nil {
		cli.Error(err.Error())
	}
	os.Exit(0)
}
